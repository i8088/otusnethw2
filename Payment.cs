﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusNetHw2
{
    public class Payment
    {
        public decimal Tariff { get; }

        public decimal Sum { private set; get; }

        public decimal Count { get; }

        public Payment(decimal count, decimal tariff)
        {
            Tariff = tariff;
            Count = count;

            Calculate();
        }

        public Payment(decimal count, decimal tariff, decimal sum)
        {
            Count = count;
            Tariff = tariff;
            Sum = sum;
        }

        private void Calculate()
        {
            Sum = Count * Tariff;
        }

        public static Payment operator +(Payment a, Payment b)
        {
            return new Payment(a.Count + b.Count, a.Tariff + b.Tariff, a.Sum + b.Sum);   
        }

        public override string ToString()
        {
            return $"Tariff {Tariff} * Count {Count} = Sum {Sum}";
        }
    }
}
