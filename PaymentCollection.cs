﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace OtusNetHw2
{
    public class PaymentCollection<T> : IEnumerable where T : Payment
    {
        private List<T> collection = new List<T>();

        public T this[int i] => collection[i];

        public void Add(T item)
        {
            collection.Add(item);
        }

        public IEnumerator GetEnumerator()
        {
            foreach (T item in collection)
            {
                yield return item;
            }
        }
    }
}
