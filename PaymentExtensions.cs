﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusNetHw2
{
    public static class PaymentExtensions
    {
        public static string ToSimpleString(this Payment payment)
        {
            return $"{payment.Count} * {payment.Tariff} = {payment.Sum}";
        }
    }
}
