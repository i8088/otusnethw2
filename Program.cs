﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Channels;

namespace OtusNetHw2
{
    class Program
    {
        /// <summary>
        /// This is just a simple demo program that performs simple manipulations with payments
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.WriteLine("This is just a simple demo program that performs simple manipulations with payments.");
            Console.WriteLine("Please enter input data - two decimal numbers like: 'decimal' 'decimal' \n" +
                              "then press Enter two times or type 'calc' and then press Enter");

            var payments = new PaymentCollection<Payment>();

            while (true)
            {
                string inputData = Console.ReadLine();

                if (inputData == "calc" || inputData == null || inputData == "")
                {
                    break;
                }

                try
                {
                    decimal[] inputDataArr = inputData.Split(" ").Select(s => decimal.Parse(s)).ToArray();

                    payments.Add(new Payment(inputDataArr[0], inputDataArr[1]));
                }
                catch (Exception e) when (e is NullReferenceException || e is IndexOutOfRangeException || e is FormatException)
                {
                    Console.WriteLine("Please, enter two decimal numbers.");
                }
            }


            PrintData(payments);
        }

        static void PrintData(PaymentCollection<Payment> payments)
        {
            Payment allPayments = null;

            foreach (Payment payment in payments)
            {
                Console.WriteLine(payment);
                Console.WriteLine(payment.ToSimpleString());

                if (allPayments == null)
                {
                    allPayments = payment;
                }
                else
                {
                    allPayments = allPayments + payment;
                }
            }

            Console.WriteLine($"All Payments: {allPayments}");
        }
    }
}
